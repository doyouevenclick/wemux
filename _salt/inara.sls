include:
- install

/usr/local/etc/wemux.conf:
  file.managed:
    - contents: |
        host_groups=(wheel wemux)
        allow_pair_mode="true"
        allow_rogue_mode="true"
        default_client_mode="mirror"
        allow_kick_user="true"
        allow_user_list="true"
        announce_attach="true"
        socket_prefix="/tmp/wemux"
        options="-u"

wemux:
  group.present:
    - system: true
