/usr/bin/wemux:
  file.managed:
    - source: salt://_artifacts/wemux
    - mode: 0755

/usr/share/man/man1/wemux.1:
  file.managed:
    - source: salt://_artifacts/wemux.1
    - mode: 0644
